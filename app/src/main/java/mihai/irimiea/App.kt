

package mihai.irimiea

import android.app.Application
import mihai.irimiea.rates.BuildConfig
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            // Release reporting
        }
    }
}