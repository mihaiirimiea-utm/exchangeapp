

package mihai.irimiea.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import mihai.irimiea.di.DaggerViewModelInjector
import mihai.irimiea.di.NetworkModule
import mihai.irimiea.di.ViewModelInjector

abstract class BaseAndroidViewModel(application: Application) : AndroidViewModel(application) {

    private val injector: ViewModelInjector =
        DaggerViewModelInjector.builder().networkModule(NetworkModule).build()

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is RateListViewModel -> injector.inject(this)
        }
    }
}