

package mihai.irimiea.viewmodel

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import mihai.irimiea.model.db.AppDatabase
import java.lang.IllegalArgumentException

class ViewModelFactory(private val activity: AppCompatActivity) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RateListViewModel::class.java)) {
            val dataBase = Room.databaseBuilder(activity.applicationContext, AppDatabase::class.java, "rates").build()
            @Suppress("UNCHECKED_CAST") return RateListViewModel(
                dataBase.ratesDao(),
                activity.application
            ) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class provided.")
    }
}