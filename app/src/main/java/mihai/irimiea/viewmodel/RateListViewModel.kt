

package mihai.irimiea.viewmodel

import android.app.Application
import android.view.View
import androidx.lifecycle.MutableLiveData
import mihai.irimiea.rates.R
import mihai.irimiea.model.ExtCurrency
import mihai.irimiea.model.RateData
import mihai.irimiea.model.RatesDao
import mihai.irimiea.utils.extensions.asCurrency
import mihai.irimiea.network.RateApi
import mihai.irimiea.ui.adapter.RateListAdapter
import mihai.irimiea.utils.FORMAT_API
import mihai.irimiea.utils.extensions.default
import mihai.irimiea.utils.extensions.supportedCurrencies
import mihai.irimiea.utils.format
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject

class RateListViewModel(private val ratesDao: RatesDao, private val app: Application) : BaseAndroidViewModel(app) {

    @Inject
    lateinit var rateApi: RateApi

    private val subscription = CompositeDisposable()

    val loadingVisibility: MutableLiveData<Boolean> = MutableLiveData()

    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadRates() }

    val dateEndpoint: MutableLiveData<Date> = MutableLiveData<Date>().default(Date())
    val baseCurrency: MutableLiveData<ExtCurrency> = MutableLiveData<ExtCurrency>().default(
        ExtCurrency(app, "USD")
    )

    val currMultiplier: MutableLiveData<Double> = MutableLiveData<Double>().default(1.0)

    val rateListAdapter: RateListAdapter =
        RateListAdapter(this)

    init {
        loadRates()
    }

    fun loadRates() {
        val date = dateEndpoint.value!!.format(FORMAT_API)
        val base = baseCurrency.value!!.code

        subscription.add(Observable.fromCallable {
            ratesDao.getRates(date, base) }
                .concatMap { dbRateList ->
                    if (dbRateList.isEmpty()) {
                        rateApi.getRates(date, base)
                            .concatMap { apiRates ->
                                ratesDao.insertAll(apiRates)
                                Observable.just(apiRates)
                            }
                    } else {
                        Observable.just(dbRateList[0])
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { TreeMap(it.rates) }
                .map { addMissingEur(it) }
                .map { mapToCurrencyPairList(it) }
                .doOnSubscribe { onStart() }
                .doOnTerminate { onComplete() }
                .subscribe(
                    { onSuccess(it) },
                    { onError(mapToCurrencyPairList(generateFromEmptyMap())) }
                ))
    }

    private fun addMissingEur(rates: TreeMap<String, Double>): TreeMap<String, Double> {
        if (rates["EUR"] == null) { rates["EUR"] = 1.0 }
        return rates
    }

    private fun mapToCurrencyPairList(rates: TreeMap<String, Double>): List<RateData> {
        val rateList = ArrayList<RateData>()
        rates.forEach {
            val currency = ExtCurrency(app, it.key)
            rateList.add(
                RateData(
                    currency,
                    (it.value * (currMultiplier.value ?: 1.0)).asCurrency(currency),
                    it.value
                )
            )
        }

        return rateList
    }

    private fun onStart() {
        loadingVisibility.value = true
        errorMessage.value = null
    }

    private fun onComplete() {
        loadingVisibility.value = false
    }

    private fun onSuccess(rateList: List<RateData>) {
        rateListAdapter.updateList(rateList)
    }

    private fun onError(rateList: List<RateData>) {
        errorMessage.value = R.string.error_loading_rates
        rateListAdapter.updateList(rateList)
    }

    private fun generateFromEmptyMap(): TreeMap<String, Double> {
        val map = TreeMap<String, Double>()
        supportedCurrencies.forEach { map[it] = 0.0 }
        return map
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}