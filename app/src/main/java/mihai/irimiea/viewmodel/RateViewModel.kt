

package mihai.irimiea.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class RateViewModel : ViewModel() {

    private val exchangeRate = MutableLiveData<String>()
    private val exchangeBase = MutableLiveData<String>()

    fun bind(rate: String, base: String) {
        exchangeRate.value = rate
        exchangeBase.value = base
    }

    fun getExchangeRate(): MutableLiveData<String> = exchangeRate

    fun getExchangeBase(): MutableLiveData<String> = exchangeBase
}