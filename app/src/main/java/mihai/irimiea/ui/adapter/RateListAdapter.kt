

package mihai.irimiea.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import mihai.irimiea.rates.R
import mihai.irimiea.rates.databinding.ItemRateBinding
import mihai.irimiea.model.RateData
import mihai.irimiea.viewmodel.RateListViewModel
import mihai.irimiea.viewmodel.RateViewModel

class RateListAdapter(private val rateListViewModel: RateListViewModel) : RecyclerView.Adapter<RateListAdapter.ViewHolder>() {

    private lateinit var exchangeList: List<RateData>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemRateBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_rate, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(exchangeList[position])
    }

    override fun getItemCount(): Int = if (::exchangeList.isInitialized) exchangeList.size else 0

    fun updateList(rateList: List<RateData>) {
        exchangeList = rateList
        notifyDataSetChanged()
    }

    override fun getItemId(position: Int): Long {
        return exchangeList[position].currency.code.hashCode().toLong()
    }

    inner class ViewHolder(private val binding: ItemRateBinding) : RecyclerView.ViewHolder(binding.root) {

        private val viewModel = RateViewModel()

        fun bind(data: RateData) {
            viewModel.bind(
                data.output,
                "1 %s = %.2f %s".format(rateListViewModel.baseCurrency.value?.code, data.rate, data.currency.code)
            )

            binding.viewModel = viewModel
            binding.root.setOnClickListener { rateListViewModel.baseCurrency.value = data.currency }

            binding.flagView.setImageResource(data.currency.flagResource)
            binding.rateCode.text = data.currency.code
            binding.rateCodeDesc.text = data.currency.displayName
        }
    }
}