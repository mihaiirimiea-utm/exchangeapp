

package mihai.irimiea.utils

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

const val FORMAT_API = "yyyy-MM-dd"
const val FORMAT_FRIENDLY = "MMM, EEE d"

private fun dateFormat(format: String): DateFormat = SimpleDateFormat(format, Locale.getDefault())

fun Date.format(format: String): String = dateFormat(format).format(this)