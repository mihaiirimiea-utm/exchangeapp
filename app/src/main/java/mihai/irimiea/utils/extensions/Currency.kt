

package mihai.irimiea.utils.extensions

import android.content.Context
import androidx.annotation.DrawableRes
import mihai.irimiea.model.ExtCurrency
import java.text.NumberFormat
import java.util.*

val supportedCurrencies = arrayOf(
    "HRK", "HUF", "IDR", "PHP", "TRY", "RON", "ISK", "SEK", "THB", "PLN",
    "GBP", "CAD", "AUD", "MYR", "NZD", "CHF", "DKK", "SGD", "CNY", "BGN",
    "CZK", "BRL", "JPY", "KRW", "INR", "MXN", "RUB", "HKD", "USD", "ZAR",
    "ILS", "NOK", "EUR"
)

@DrawableRes fun Currency.flagDrawable(context: Context): Int =
    context.resources.getIdentifier("ic_flag_${currencyCode.toLowerCase()}", "drawable", context.packageName)

fun Double.asCurrency(currency: ExtCurrency): String = this.asCurrency(currency.code)

fun Double.asCurrency(code: String): String {
    val instance = NumberFormat.getCurrencyInstance()
    instance.currency = Currency.getInstance(code)
    return instance.format(this)
}