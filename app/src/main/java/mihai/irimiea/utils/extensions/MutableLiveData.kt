
package mihai.irimiea.utils.extensions

import androidx.lifecycle.MutableLiveData

inline fun <reified T> MutableLiveData<T>.default(initialValue: T) = apply { value = initialValue }