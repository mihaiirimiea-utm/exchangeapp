

package mihai.irimiea.utils.extensions

import android.graphics.BitmapFactory
import android.graphics.Color
import androidx.annotation.DrawableRes
import com.google.android.material.chip.Chip
import mihai.irimiea.ui.graphics.CircleImageDrawable

fun Chip.setRoundedIcon(@DrawableRes icon: Int) {
    val iconBitmap = BitmapFactory.decodeResource(resources, icon)

    /*val roundedDrawable = RoundedBitmapDrawableFactory.create(resources, iconBitmap)
    roundedDrawable.isCircular = true*/
    val roundedDrawable =
        CircleImageDrawable(iconBitmap, Color.argb(255, 255, 255, 255), 4)

    chipIcon = roundedDrawable
}