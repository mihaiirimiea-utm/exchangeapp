

package mihai.irimiea.utils

import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.ContentLoadingProgressBar
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import mihai.irimiea.model.ExtCurrency
import mihai.irimiea.utils.extensions.asCurrency
import mihai.irimiea.utils.extensions.getParentActivity
import mihai.irimiea.utils.extensions.setRoundedIcon
import mihai.irimiea.viewmodel.RateListViewModel
import java.util.*

fun <T> MutableLiveData<T>?.mutableObserver(view: View, observer: (T) -> Unit) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && this != null) {
        observe(parentActivity, Observer {
            observer.invoke(it)
        })
    }
}

@BindingAdapter("mutableVisibility")
fun setMutableVisibility(progressBar: ContentLoadingProgressBar, visibility: MutableLiveData<Boolean>?) {
    visibility.mutableObserver(progressBar as View) {
        if (it) { progressBar.show() } else { progressBar.hide() }
    }
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
    text.mutableObserver(view) { view.text = it }
}

@BindingAdapter("mutableCurrencyValue")
fun setMutableCurrencyValue(view: Chip, viewModel: RateListViewModel) {
    viewModel.currMultiplier.mutableObserver(view) {
        view.text = it.asCurrency(viewModel.baseCurrency.value!!)
        viewModel.baseCurrency.value = viewModel.baseCurrency.value
    }

    viewModel.baseCurrency.mutableObserver(view) { view.text = viewModel.currMultiplier.value!!.asCurrency(it) }
}

@BindingAdapter("mutableChipIcon")
fun setMutableChipIcon(view: Chip, currency: MutableLiveData<ExtCurrency>?) {
    currency.mutableObserver(view) { view.setRoundedIcon(it.flagResource) }
}

@BindingAdapter("mutableYear")
fun setMutableYear(view: TextView, date: MutableLiveData<Date>?) {
    date.mutableObserver(view) {
        val calendar = GregorianCalendar()
        calendar.time = it

        view.text = calendar.get(Calendar.YEAR).toString()
    }
}

@BindingAdapter("mutableFriendlyDate")
fun setMutableFriendlyDate(view: TextView, date: MutableLiveData<Date>?) {
    date.mutableObserver(view) { view.text = it.format(FORMAT_FRIENDLY) }
}


@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    if (!adapter.hasStableIds()) adapter.setHasStableIds(true)

    view.setHasFixedSize(true)
    view.layoutManager = LinearLayoutManager(view.context)
    view.adapter = adapter
}