

package mihai.irimiea.network

import mihai.irimiea.model.Rates
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface RateApi {

    @GET
    fun getRates(@Url endpoint: String, @Query("base") base: String): Observable<Rates>
}