

package mihai.irimiea.model

data class RateData(val currency: ExtCurrency, val output: String, val rate: Double)