

package mihai.irimiea.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface RatesDao {

    @get:Query("SELECT * FROM rates")
    val all: List<Rates>

    @Query("SELECT * FROM rates WHERE base LIKE :baseCode AND date LIKE :date")
    fun getRates(date: String, baseCode: String): List<Rates>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg rates: Rates)
}