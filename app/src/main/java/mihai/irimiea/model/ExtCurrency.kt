

package mihai.irimiea.model

import android.content.Context
import androidx.annotation.DrawableRes
import mihai.irimiea.utils.extensions.flagDrawable
import java.util.*

class ExtCurrency(context: Context, val code: String) {

    val displayName: String

    val symbol: String

    @DrawableRes
    val flagResource: Int

    init {
        val currency = Currency.getInstance(code)
        displayName = currency.displayName
        symbol = currency.symbol
        flagResource = currency.flagDrawable(context)
    }
}