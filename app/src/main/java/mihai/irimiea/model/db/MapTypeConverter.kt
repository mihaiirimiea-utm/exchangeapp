

package mihai.irimiea.model.db

import androidx.room.TypeConverter

object MapTypeConverter {

    @TypeConverter
    @JvmStatic
    fun mapToString(value: Map<String, Double>?): String? {
        val builder = StringBuilder()
        value?.forEach {
            builder.append(it.key)
            builder.append("=")
            builder.append(it.value)
            builder.append(";")
        } ?: return null

        builder.setLength(builder.length - 1)
        return builder.toString()
    }

    @TypeConverter
    @JvmStatic
    fun stringToMap(value: String?): Map<String, Double>? {
        return value?.split(";")?.associate {
            val (key, floatValue) = it.split("=")
            key to floatValue.toDouble()
        }
    }
}