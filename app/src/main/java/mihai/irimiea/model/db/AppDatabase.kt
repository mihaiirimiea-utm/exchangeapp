

package mihai.irimiea.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import mihai.irimiea.model.Rates
import mihai.irimiea.model.RatesDao

@Database(entities = [Rates::class], version = 1, exportSchema = false)
@TypeConverters(MapTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun ratesDao(): RatesDao
}