

package mihai.irimiea.model

import androidx.room.Entity

@Entity(primaryKeys = ["base", "date"])
data class Rates(val rates: Map<String, Double>, val base: String, val date: String)