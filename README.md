# Dezvoltarea Aplicatiilor Mobile - UTM #
* Proiect - Lista de exchange rate a celor mai importante valute

# Cerinte atinse

* Kotlin
* UI layouts and views
* MVVM architecture
* Retrofit
* [OpenRates](http://openrates.io/) api for data
* Dagger
* Android Jetpack (using AndroidX)
  * Room
  * Data Binding
  * Architecture Components
